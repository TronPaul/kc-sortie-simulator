(ns kc-sortie-simulator.core-test
  (:refer-clojure :exclude [rand rand-int rand-nth rand-nth-or-nil])
  (:require [clojure.test :refer :all]
            [kc-sortie-simulator.core :as core]
            [kc-sortie-simulator.ship :as ship]
            [kc-sortie-simulator.random :refer :all]))

(deftest choose-engagement-test
  (testing "player equipment can block red t"
    (with-redefs [rand (constantly 0.91)]
      (is (= :head-on (core/choose-engagement {:kcss/ships [{:kcss/equipments [{:kcss/block-red-t true}]}]} {})))))
  (testing "enemy equipment can block red t"
    (with-redefs [rand (constantly 0.91)]
      (is (= :head-on (core/choose-engagement {} {:kcss/ships [{:kcss/equipments [{:kcss/block-red-t true}]}]}))))))

(deftest shelling?-test
  (testing "shelling not allowed at non-battle node"
    (is (not (core/shelling? {:kcss/node-type :kcss/air-battle-node
                              :kcss/player-fleet {:kcss/ships [{:kcss/current-health 1}]}
                              :kcss/enemy-fleet {:kcss/ships [{:kcss/current-health 1}]}}))))
  (testing "shelling not allowed if enemy is dead"
    (is (not (core/shelling? {:kcss/node-type :kcss/battle-node
                              :kcss/player-fleet {:kcss/ships [{:kcss/current-health 1}]}
                              :kcss/enemy-fleet {:kcss/ships [{:kcss/current-health 0}]}}))))
  (testing "shelling not allowed if player is dead"
    (is (not (core/shelling? {:kcss/node-type :kcss/battle-node
                              :kcss/player-fleet {:kcss/ships [{:kcss/current-health 0}]}
                              :kcss/enemy-fleet {:kcss/ships [{:kcss/current-health 1}]}}))))
  (testing "shelling allowed otherwise"
    (is (core/shelling? {:kcss/node-type :kcss/battle-node
                         :kcss/player-fleet {:kcss/ships [{:kcss/current-health 1}]}
                         :kcss/enemy-fleet {:kcss/ships [{:kcss/current-health 1}]}}))))

(deftest select-defending-ship-idx-test
  (testing "selects non-flagship if flagship protection triggers"
    (with-redefs [rand (constantly 0.5)
                  rand-nth (constantly 0)
                  ship/can-asw? (constantly false)]
      (is (= 0 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 1
                                                                                :kcss/ship-type :bb}
                                                                               {:kcss/current-health 1
                                                                                :kcss/ship-type :bb}]
                                                                  :kcss/formation :line-ahead}
                                                :defending-side :enemy
                                                :searchlight-definition nil})))))
  (testing "selects alive flagship"
    (with-redefs [rand-nth (constantly 0)
                  ship/can-asw? (constantly false)]
      (is (= 0 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 1
                                                                                :kcss/ship-type :bb}
                                                                               {:kcss/current-health 0
                                                                                :kcss/ship-type :bb}]
                                                                  :kcss/formation :line-ahead}
                                                :defending-side :enemy
                                                :searchlight-definition nil})))))
  (testing "selects alive non-flagship"
    (is (= 1 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 0
                                                                              :kcss/ship-type :bb}
                                                                             {:kcss/current-health 1
                                                                              :kcss/ship-type :bb}]
                                                                :kcss/formation :line-ahead}
                                              :defending-side :enemy
                                              :searchlight-definition nil}))))
  (testing "returns nil with no available targets"
    (is (nil? (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 0
                                                                               :kcss/ship-type :bb}
                                                                              {:kcss/current-health 0
                                                                               :kcss/ship-type :bb}]
                                                                 :kcss/formation :line-ahead}
                                               :defending-side :enemy
                                               :searchlight-definition nil}))))
  (testing "returns a sub if can-asw"
    (with-redefs [ship/can-asw? (constantly true)]
      (is (= 0 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 1
                                                                                :kcss/ship-type :ss}
                                                                               {:kcss/current-health 1
                                                                                :kcss/ship-type :bb}]
                                                                  :kcss/formation :line-ahead}
                                                :defending-side :enemy
                                                :searchlight-definition nil})))))
  (testing "returns a non-flagship sub if can-asw and 2 subs exist"
    (with-redefs [rand (constantly 0.5)
                  rand-nth (constantly 0)
                  ship/can-asw? (constantly true)]
      (is (= 0 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 1
                                                                                :kcss/ship-type :ss}
                                                                               {:kcss/current-health 1
                                                                                :kcss/ship-type :ssv}]
                                                                  :kcss/formation :line-ahead}
                                                :defending-side :enemy
                                                :searchlight-definition nil})))))
  (testing "rerolls to select searchlight ship if searchlight is set"
    (let [ret (ref 0)]
      (dosync
        (with-redefs [rand-nth (fn [& _]
                                 (let [r @ret]
                                   (alter ret inc)
                                   r))
                      ship/can-asw? (constantly false)]
          (is (= 1 (core/select-defending-ship-idx {:defending-fleet {:kcss/ships [{:kcss/current-health 1
                                                                                    :kcss/ship-type :bb}
                                                                                   {:kcss/current-health 1
                                                                                    :kcss/ship-type :bb}]
                                                                      :kcss/formation :line-ahead}
                                                    :defending-side :enemy
                                                    :searchlight-definition {:kcss/ship-index 1
                                                                             :kcss/rolls 1}}))))))))
