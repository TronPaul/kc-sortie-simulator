(ns kc-sortie-simulator.combat-test
  (:require [clojure.test :refer :all])
  (:use [kc-sortie-simulator.combat]))

(deftest evasion-test
  (testing "evasion without bonus, fuel penalty, or vanguard"
    (are [expected actual] (= expected actual)
                           0.03 (evasion {:defending-ship {:kcss/ship-stats {:kcss/luck 2 :kcss/evasion 1}
                                                          :kcss/resources {:kcss/fuel 10}}
                                         :defending-ship-idx 0
                                         :defending-formation :line-ahead})
                           0.182 (evasion  {:defending-ship {:kcss/ship-stats {:kcss/luck 8 :kcss/evasion 10}
                                                            :kcss/resources {:kcss/fuel 10}}
                                           :defending-ship-idx 0
                                           :defending-formation :echelon})
                           0.15115823125451335 (evasion {:defending-ship {:kcss/ship-stats {:kcss/luck 7 :kcss/evasion 10}
                                                                         :kcss/resources {:kcss/fuel 10}}
                                                        :defending-ship-idx 0
                                                        :defending-formation :diamond}))))


