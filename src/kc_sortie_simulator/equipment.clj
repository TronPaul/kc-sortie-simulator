(ns kc-sortie-simulator.equipment)

(defn is-wg?
  [equipment]
  (= 126 (:kcss/global-id equipment)))

(defn is-mortar?
  [equipment]
  (= 346 (:kcss/global-id equipment)))

(defn is-mortar-concentrated?
  [equipment]
  (= 347 (:kcss/global-id equipment)))

(defn is-mortar-incl-concentrated?
  [equipment]
  (or (is-mortar? equipment) (is-mortar-concentrated? equipment)))

(defn is-type-4-rocket?
  [equipment]
  (= 348 (:kcss/global-id equipment)))

(defn is-m4a1?
  [equipment]
  (= 355 (:kcss/global-id equipment)))

(defn is-daihatsu?
  [equipment]
  (contains? #{68 193} (:kcss/global-id equipment)))

(defn is-daihatsu-with-tank?
  [equipment]
  (= 166 (:kcss/global-id equipment)))

(defn is-landing-tank?
  [equipment]
  (= 167 (:kcss/global-id equipment)))

(defn is-toku-daihatsu?
  [equipment]
  (= 193 (:kcss/global-id equipment)))

(defn is-toku-daihatsu-with-11th?
  [equipment]
  (= 230 (:kcss/global-id equipment)))

(defn is-any-daihatsu?
  [equipment]
  (some #(% equipment) [is-daihatsu? is-daihatsu-with-tank? is-toku-daihatsu? is-toku-daihatsu-with-11th?]))

(defn is-type-3-shell?
  [equipment]
  false)

(defn is-ap-shell?
  [equipment]
  false)

(defn is-dive-bomber?
  [equipment]
  false)

(defn is-seaplane-fighter-or-bomber?
  [equipment]
  false)

(defn count-stars-of
  [pred equipments]
  (reduce + (filter pred equipments)))