(ns kc-sortie-simulator.core
  (:refer-clojure :exclude [rand rand-int rand-nth rand-nth-or-nil])
  (:require [clojure.spec.alpha :as s]
            [clojure.set]
            [clojure.string]
            [kc-sortie-simulator.util :refer [flatten-map unflatten cond->->]]
            [kc-sortie-simulator.combat :as combat]
            [clojure.tools.logging :as log]
            [kc-sortie-simulator.ship :as ship]
            [kc-sortie-simulator.equipment :as equip]
            [kc-sortie-simulator.random :refer :all])
  (:gen-class))

(s/def :kcss/global-id (s/and int? #(< -1)))

(s/def :kcss/chance (s/and double? #(> 1) #(< 0)))


(s/def :kcss/plane (s/keys :req [:kcss/global-id :kcss/plane-stats]))

(s/def :kcss/planes (s/coll-of :kcss/plane))

(s/def :kcss/raid-squadrons (s/coll-of :kcss/planes))

(s/def :kcss/ships (s/coll-of :kcss/ship))

(s/def :kcss/formation #{:line-abreast :line-ahead :echelon :vanguard :double-line :diamond})

(s/def :kcss/fleet (s/keys :req [:kcss/ships :kcss/formation :kcss/chance] :opt [:kcss/enemy-planes]))

(s/def :kcss/fleets (s/coll-of :kcss/fleet))

(s/def :kcss/friend-fleet (s/keys :req [:kcss/ships :kcss/formation] :opt [:kcss/chance])) ; TODO either all or none must have chance

(s/def :kcss/friend-fleets (s/coll-of :kcss/friend-fleet))

(s/def :kcss/night-battle? boolean?)

(defmulti node :kcss/node-type)
(defmethod node :kcss/empty-node [_]
  nil?)
(defmethod node :kcss/air-raid-node [_]
  (s/keys :req [:kcss/raid-squadrons]))  ; TODO: does the enemy fleet matter here?
(defmethod node :kcss/air-battle-node [_]
  (s/keys :req [:kcss/raid-squadrons :kcss/fleets]))
(defmethod node :kcss/battle-node [_]
  (s/keys :req [:kcss/fleets :kcss/night-battle?] :opt [:kcss/friend-fleets]))
(defmethod node :kcss/night-battle-node [_]
  (s/keys :req [:kcss/fleets :kcss/night-battle?] :opt [:kcss/friend-fleets]))

(s/multi-spec node :kcss/node)

(s/def :kcss/node-name string?)

(s/def :kcss/nodes (s/map-of :kcss/node-name :kcss/node))

(s/def :kcss/node-names (s/coll-of :kcss/node-name))

(s/def :kcss/lbas-raids (s/keys :req [:kcss/raid-squadrons :kcss/node-names]))

(s/def :kcss/map (s/keys :req [:kcss/nodes :kcss/lbas-raids]))

(s/def :kcss/player-fleet :kcss/fleet)
(s/def :kcss/enemy-fleet :kcss/fleet)
(s/def :kcss/lbas-setting #{:sortie :defense :rest :standby :retreat})
(s/def :kcss/lbas (s/keys :req [:kcss/planes :kcss/lbas-setting] :opt [:kcss/node-name]))
(s/def :kcss/lbases (s/coll-of :kcss/lbas))
(s/def :kcss/player-lbases :kcss/lbases)
(s/def :kcss/enemy-planes :kcss/planes)
(s/def :kcss/battle (s/keys :req [:kcss/player-fleet :kcss/enemy-fleet :kcss/player-lbases :kcss/enemy-planes]))

(def flag-protection
  {:player {:line-ahead 0.452
            :double-line 0.596
            :diamond 0.714
            :echelon 0.5
            :line-abreast 0.622}
   :enemy {:line-ahead 0.431
           :double-line 0.567
           :diamond 0.706
           :echelon 0.602
           :line-abreast 0.587}})

(defn- other-side
  [side]
  (if (= :player side)
    :enemy
    :player))

(defn block-red-t?
  [{ships :kcss/ships}]
  (some :kcss/block-red-t (flatten (map :kcss/equipments ships))))

(defn choose-engagement
  [player-fleet enemy-fleet]
  (let [r (rand)]
    (log/debug "Rolled roll=" r " for engagement")
    (cond
      (< r 0.45) :parallel
      (< r 0.6) :green-t
      (< r 0.9) :head-on
      :else (if (some block-red-t? [player-fleet enemy-fleet])
              :head-on
              :red-t))))

; fleets should be sorted by chance
(defn choose-enemy-fleet
  [{fleets :kcss/fleets}]
  (let [r (rand)]
    (first (filter #(< r (:kcss/chance %)) fleets))))

(def lbas-support?
  (constantly false))

(def do-lbas-support
  identity)

(def node-support?
  (constantly false))

(def do-node-support
  identity)

(def jet-lbas-support?
  (constantly false))

(def do-jet-lbas-support
  identity)

(def jet-air-strike?
  (constantly false))

(def do-jet-air-strike
  identity)

(def air-strike?
  (constantly false))

(def do-air-strike
  identity)

(def second-air-strike?
  (constantly false))

(def do-second-air-strike
  identity)

(def opening-asw?
  (constantly false))

(def do-opening-asw
  identity)

(def opening-torpedo?
  (constantly false))

(def do-opening-torpedo
  identity)

(defn shelling?
  [{node-type :kcss/node-type
    player-fleet :kcss/player-fleet
    enemy-fleet :kcss/enemy-fleet}]
  (and (= :kcss/battle-node node-type) (some ship/is-alive? (:kcss/ships player-fleet)) (some ship/is-alive? (:kcss/ships enemy-fleet))))

(defn select-attacking-ship-idx
  [ships attack-range]
  (rand-nth-or-nil (filter #(and (ship/is-alive? (nth ships %)) (= attack-range (ship/range (get ships %)))) (range (count ships)))))

(defn select-defending-ship-idx
  [{{ships :kcss/ships formation :kcss/formation} :defending-fleet attacking-ship :attacking-ship side :defending-side searchlight-definition :searchlight-definition}]
  (let [flag-protection (get-in flag-protection [side formation])
        has-dive-bomber (ship/has-equipment? equip/is-dive-bomber? attacking-ship)
        can-asw (ship/can-asw? attacking-ship)
        alive-ship-idxs (if-let [alive-sub-idxs (seq (filter #(and can-asw (ship/is-sub? (nth ships %)) (ship/is-alive? (nth ships %))) (range (count ships))))]
                          alive-sub-idxs
                          (if-let [installation-idxs (seq (filter #(and has-dive-bomber (ship/is-installation? (nth ships %)) (ship/is-alive? (nth ships %))) (range (count ships))))]
                            installation-idxs
                            (filter #(ship/is-alive? (nth ships %)) (range (count ships)))))]
    (let [idx (if (nil? searchlight-definition)
                (rand-nth-or-nil alive-ship-idxs)
                (loop [rolls (:kcss/rolls searchlight-definition)
                       idx (rand-nth-or-nil alive-ship-idxs)]
                  (if (or (= idx (:kcss/ship-index searchlight-definition)) (= rolls 0))
                    idx
                    (recur (dec rolls) (rand-nth-or-nil alive-ship-idxs)))))]
      (if (and (= 0 idx) (< 1 (count alive-ship-idxs)) (> flag-protection (rand)))
        (rand-nth (rest alive-ship-idxs))
        idx))))

(defn do-turn
  [{attacking-fleet :attacking-fleet
    defending-fleet :defending-fleet
    cur-range :cur-range
    :as turn-definition}]
  (if-let [attacking-ship-idx (select-attacking-ship-idx (get attacking-fleet :kcss/ships) cur-range)]
    (let [attacking-ship (nth (get attacking-fleet :kcss/ships) attacking-ship-idx)
          attacking-definition {:attacking-ship attacking-ship :attacking-ship-idx attacking-ship-idx}]
      (let [special-attack (combat/special-attack (merge turn-definition attacking-definition))]
        (if (:attack special-attack)
          (let [attacking-ships (map #(nth (get attacking-fleet :kcss/ships) %) (:attacking-ship-indexes special-attack))]
            (loop [attacking-ship (first attacking-ships)
                   attacking-ships (rest attacking-ships)
                   defending-fleet defending-fleet
                   attacks []]
              (let [defending-ship-idx (assoc turn-definition :attacking-ship attacking-ship)
                    defending-ship (nth (get defending-fleet :kcss/ships) defending-ship-idx)
                    defending-definition {:defending-ship defending-ship :defending-ship-idx defending-ship-idx}
                    attack-definition (merge turn-definition attacking-definition defending-definition)
                    hit-type (combat/hit-type attack-definition)
                    hit-bonus (combat/hit-bonus (merge attack-definition hit-type))
                    attack (merge (combat/damage (merge attack-definition special-attack {:hit-bonus hit-bonus})) hit-type)
                    updated-defending-fleet (update-in defending-fleet [:kcss/ships defending-ship-idx :kcss/current-health] #(Math/max 0 (- % (:damage attack))))
                    updated-attacking-fleet (assoc-in attacking-fleet [:kcss/ships attacking-ship-idx :special-attacked] true)]
                (if (seq attacking-ships)
                  (recur (first attacking-ships) (rest attacking-ships) updated-defending-fleet (conj attacks attack))
                  {:defending-fleet updated-defending-fleet
                   :attacking-fleet updated-attacking-fleet
                   :attacks attacks}))))
          (let [defending-ship-idx (select-defending-ship-idx (assoc turn-definition :attacking-ship attacking-ship))
                defending-ship (nth (get defending-fleet :kcss/ships) defending-ship-idx)
                defending-definition {:defending-ship defending-ship :defending-ship-idx defending-ship-idx}
                attack (combat/get-attack (merge turn-definition attacking-definition defending-definition))
                attack-definition (merge turn-definition attacking-definition defending-definition {:attack attack})
                attacks (map (fn [_]
                               (let [hit-type (combat/hit-type attack-definition)
                                     hit-bonus (combat/hit-bonus (merge attack-definition hit-type))]
                                 (merge (combat/damage (assoc attack-definition :hit-bonus hit-bonus)) hit-type))) (:attacks attack))
                updated-defending-fleet (update-in defending-fleet [:kcss/ships defending-ship-idx :kcss/current-health] #(Math/max 0 (- % (reduce + (map :damage attacks)))))]
            {:defending-fleet updated-defending-fleet
             :attacks attacks})))
      (assoc-in turn-definition [:attacking-fleet :kcss/fleet attacking-ship-idx :attacked] true))
    turn-definition))

(defn do-shelling
  [{player-fleet :kcss/player-fleet
    enemy-fleet :kcss/enemy-fleet
    :as shelling-definition}]
  (loop [fleets {:player player-fleet :enemy enemy-fleet}
         cur-side :player
         cur-range :very-long
         ranges [:long :medium :short]]                     ; TODO include log of actions (rolls, damage etc)
    (let [turn-definition (merge shelling-definition {:attacking-fleet (get fleets cur-side)
                                                      :defending-fleet (get fleets (other-side cur-side))
                                                      :cur-side cur-side
                                                      :cur-range cur-range})
          updated-fleets (do-turn turn-definition)]
      (cond
        (not-empty (filter #(ship/can-attack? % cur-range) (get-in fleets [(other-side cur-side) :kcss/ships]))) (recur updated-fleets (other-side cur-side) cur-range ranges)
        (not-empty (filter #(ship/can-attack? % cur-range) (get-in fleets [cur-side :kcss/ships]))) (recur updated-fleets cur-side cur-range ranges)
        (not-empty ranges) (recur updated-fleets :player (first ranges) (rest ranges))
        :else updated-fleets))))

(def second-shelling?
  (constantly false))

(def do-second-shelling
  identity)

(def closing-torpedo?
  (constantly false))

(def do-closing-torpedo
  identity)

(def friend-fleet?
  (constantly false))

(def do-friend-fleet
  identity)

(def night-battle?
  (constantly false))

(def do-night-battle
  identity)

(def apply-morale
  identity)

(defn simulate-node [sortie-definition node]
  (let [enemy-fleet (choose-enemy-fleet node)
        engagement (choose-engagement (:kcss/player-fleet sortie-definition) enemy-fleet)
    node-sortie-definition {:kcss/player-fleet (:kcss/player-fleet sortie-definition)
                            :kcss/enemy-fleet enemy-fleet
                            :kcss/node-type (:kcss/node-type node)
                            :kcss/engagement engagement}]
        (let [post-battle-definition (-> node-sortie-definition
                                         (cond->->
                                           (jet-lbas-support?) (do-jet-lbas-support)
                                           (jet-air-strike?) (do-jet-air-strike)
                                           (lbas-support?) (do-lbas-support)
                                           (air-strike?) (do-air-strike)
                                           (second-air-strike?) (do-second-air-strike)
                                           (node-support?) (do-node-support)
                                           (opening-asw?) (do-opening-asw)
                                           (opening-torpedo?) (do-opening-torpedo)
                                           (shelling?) (do-shelling)
                                           (second-shelling?) (do-second-shelling)
                                           (closing-torpedo?) (do-closing-torpedo)
                                           (friend-fleet?) (do-friend-fleet)
                                           (night-battle?) (do-night-battle))
                                         (apply-morale))]
          {:kcss/player-fleet (:kcss/player-fleet post-battle-definition)})))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
