(ns kc-sortie-simulator.random
  (:refer-clojure :exclude [rand rand-int rand-nth rand-nth-or-nil])
  (:import (java.util Random)))

(def ^:dynamic ^Random
  *rnd*
  "Random instance for use in generators. By consistently using this
instance you can get a repeatable basis for tests."
  (Random.))

(defn rand
  ([]
   (.nextDouble *rnd*))
  ([n]
   (* (.nextDouble *rnd*))))

(defn rand-int
  [n]
  (int (rand n)))

(defn rand-nth
  [coll]
  (nth coll (rand-int (count coll))))

(defn rand-nth-or-nil
  [coll]
  (if (empty? coll)
    nil
    (rand-nth coll)))
