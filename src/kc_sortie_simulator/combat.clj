(ns kc-sortie-simulator.combat
  ^{:doc "Functions and static data for combat calculations

  Any function taking more than one argument should take in the entire
  attack definition."}
  (:require [kc-sortie-simulator.ship :as ship]
            [kc-sortie-simulator.fleet :as fleet]
            [kc-sortie-simulator.equipment :as equip]))

(def engagement-damage-modifier
  {:green-t 1.2
   :parallel 1
   :head-on 0.8
   :red-t 0.6})

(def shelling-evasion-modifier
  {:line-ahead 1
   :double-line 1
   :diamond 1.1
   :echelon 1.3
   :line-abreast 1.3})

(def shelling-formation-damage-modifier
  {:line-ahead 1
   :double-line 0.8
   :diamond 0.7
   :echelon 0.75
   :line-abreast 0.6})

(defn formation-countered?
  [{attacking-formation :attacking-formation
    defending-formation :defending-formation}]
  (contains? #{[:double-line :line-abreast]
               [:echelon :line-ahead
                :line-abreast :echelon]}
             [attacking-formation defending-formation]))

(defn shelling-formation-accuracy
  [{formation :attacking-formation
    idx :attacking-ship-index
    :as attack-definition}]
  (if (not (formation-countered? attack-definition))
    (get {:line-ahead 1
          :double-line 1.2
          :diamond 1
          :echelon 1.2
          :line-abreast 0.8} formation 0)
    0))

(defn- as-double-percentile
  [i]
  (* i 0.01))

(defn vanguard-evasion-bonus
  [{idx :defending-ship-idx
    {ship-type :kcss/ship-type} :defending-ship
    {ships :kcss/ships} :defending-fleet
    formation :defending-formation}]
  (if (= :vanguard formation)
    (if (<= (/ idx (count ships)) 0.8)
      (if (= :dd ship-type)
        20
        5)
      (if (= :dd ship-type)
        40
        20))
    0))

(defn morale-shelling-accuracy
  [morale]
  (cond
    (>= morale 53) 1.3
    (>= morale 33) 1
    (>= morale 23) 0.7
    :else 0.35))

(defn morale-evasion-modifier
  [morale]
  (cond
    (>= morale 53) 0.7
    (>= morale 33) 1
    (>= morale 23) 1.2
    :else 1.4))

(defn ap-accuracy
  [{attacking-ship :attacking-ship
    defending-ship :defending-ship}]
  (if (ship/weak-to-ap? defending-ship)
    (let [ap-type (ship/ap-type attacking-ship)]
      (cond
        (= ap-type :all-types) 1.3
        (= ap-type :secondary) 1.2
        (= ap-type :radar) 1.25
        (= ap-type :basic) 1.1
        :else 1))
    1))

(defn pt-imp-accuracy-modifier
  [{{attacking-ship-type :kcss/ship-type
     :as attacking-ship} :attacking-ship
    {defending-ship-type :kcss/ship-type} :defending-ship}]
  (if (and (= defending-ship-type :pt-imp) (not (= attacking-ship-type :dd)) (< 1 (ship/count-equipment #(contains? #{:main-gun :main-gun-aa :secondary-gun :aa-gun} (get % :kcss/equipment-type)) attacking-ship)))
    1.5
    1))

(defn base-accuracy
  [attack-definition]
  ; TODO combined-fleet
  90)

(defn attack-accuracy
  [attack]
  "Accuracy mod for attack type (cut-in, etc)"
  1)

(defn attack-base-damage-cap
  [attack-type]
  180)

(defn ap-damage
  [{attacking-ship :attacking-ship
    defending-ship :defending-ship}]
  (if (ship/weak-to-ap? defending-ship)
    (let [ap-type (ship/ap-type attacking-ship)]
      (cond
        (= ap-type :all-types) 1.15
        (= ap-type :secondary) 1.15
        (= ap-type :radar) 1.1
        (= ap-type :basic) 1.08
        :else 1))
    1))

(defn pt-imp-damage-modifier
  [{{attacking-ship-type :kcss/ship-type
     :as attacking-ship} :attacking-ship
    {defending-ship-type :kcss/ship-type} :defending-ship}]
  (if (and (= defending-ship-type :pt-imp) (not (= attacking-ship-type :dd)) (< 1 (ship/count-equipment #(contains? #{:main-gun :main-gun-aa :secondary-gun :aa-gun} (get % :kcss/equipment-type)) attacking-ship)))
    1.2
    1))

(defn attack-damage-modifier
  "Post mod for attack type (cut-in, etc)"
  [damage-definition]
  1)

(defn installation-bonus-1
  [ship]
  (+ 1 (/ (/ (equip/count-stars-of equip/is-daihatsu? (:kcss/equipments ship)) (+ (ship/count-equipment #(or (equip/is-daihatsu? %1) (equip/is-daihatsu-with-tank? %1)) ship))) 50)))

(defn installation-bonus-3
  [ship]
  (+ 1 (/ (/ (equip/count-stars-of equip/is-landing-tank? (:kcss/equipments ship)) (ship/count-equipment equip/is-landing-tank? ship)) 30)))

(defn anchorage-damage-modifier
  [{attacking-ship :attacking-ship
    {ship-type :kcss/ship-type} :defending-ship}]
  (if (= :anchorage ship-type)
    (cond-> 1
            (ship/has-equipment? equip/is-mortar? attacking-ship) (* 1.15)
            (ship/has-equipment? equip/is-type-4-rocket? attacking-ship) (* 1.2)
            (ship/has-equipment? equip/is-wg? attacking-ship) (* 1.25)
            (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.3)
            (ship/has-equipment? equip/is-any-daihatsu? attacking-ship) (* 1.45 (installation-bonus-1 attacking-ship))
            (ship/has-equipment? equip/is-daihatsu-with-tank? attacking-ship) (* 1.15)
            (ship/has-equipment? equip/is-landing-tank? attacking-ship) (* 2.4 (installation-bonus-3 attacking-ship))
            (ship/has-equipment? equip/is-m4a1? attacking-ship) (* 1.75)
            (ship/has-equipment? equip/is-type-3-shell? attacking-ship) (* 1.35)
            (ship/has-equipment? equip/is-dive-bomber? attacking-ship) (* 1.4)
            (<= 2 (ship/count-equipment equip/is-dive-bomber? attacking-ship)) (* 1.5))
    1))

(defn shell-bonus
  [damage-definition]
  ; TODO combined-fleet
  5)

(defn debuff
  [damage-definition]
  0)

(defn wg-power
  [ship]
  (let [wg-count (ship/count-equipment equip/is-wg? ship)]
    (cond
      (= 1 wg-count) 75
      (= 2 wg-count) 110
      (= 3 wg-count) 140
      (= 4 wg-count) 160
      :else 0)))

(defn installation-flat-bonus
  [{attacking-ship :attacking-ship}]
  (if false
    (cond-> (wg-power attacking-ship)
            (ship/has-equipment? equip/is-toku-daihatsu-with-11th? attacking-ship) (+ 25)
            (<= 2 (ship/count-equipment equip/is-mortar-concentrated? attacking-ship)) (+ 110)
            (= 1 (ship/count-equipment equip/is-mortar-concentrated? attacking-ship)) (+ 60)
            (<= 3 (ship/count-equipment equip/is-mortar? attacking-ship)) (+ 75)
            (= 2 (ship/count-equipment equip/is-mortar? attacking-ship)) (+ 55)
            (= 1 (ship/count-equipment equip/is-mortar? attacking-ship)) (+ 30)
            (<= 2 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (+ 115)
            (= 1 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (+ 55))))

(defn supply-depot-damage-modifier
  [{attacking-ship :attacking-ship
    {ship-type :kcss/ship-type} :defending-ship}]
  (if (= :supply-depot ship-type)
    (cond-> 1
            (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.625)
            (= 1 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.45)
            (<= 1 (ship/count-equipment equip/is-mortar? attacking-ship)) (* 1.2)
            (<= 2 (ship/count-equipment equip/is-mortar? attacking-ship)) (* 1.4)
            (<= 1 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (* 1.15)
            (<= 2 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (* 1.2)
            (ship/has-equipment? equip/is-daihatsu-with-tank? attacking-ship) (* 2.1 (installation-bonus-1 attacking-ship))
            (ship/has-equipment? equip/is-m4a1? attacking-ship) (* 1.2)
            (<= 2 (ship/count-equipment equip/is-landing-tank? attacking-ship)) (* 2.5 (installation-bonus-3 attacking-ship))
            (= 1 (ship/count-equipment equip/is-landing-tank? attacking-ship)) (* 1.9 (installation-bonus-3 attacking-ship)))
    1))

(defn installation-type-bonus
  [{{attacking-ship-type :kcss/ship-type
     :as attacking-ship} :attacking-ship
    {defending-ship-type :kcss/ship-type} :defending-ship}]
  (cond
    (= :pill-box defending-ship-type) (cond-> 1
                                              (contains? #{:dd :cl} attacking-ship-type) (* 1.4)
                                              (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 2.72)
                                              (= 1 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.6)
                                              (ship/has-equipment? equip/is-mortar-incl-concentrated? attacking-ship) (* 1.3)
                                              (<= 2 (ship/count-equipment equip/is-mortar-incl-concentrated? attacking-ship)) (* 1.5)
                                              (ship/has-equipment? equip/is-type-4-rocket? attacking-ship) (* 1.5)
                                              (<= 2 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (* 1.8)
                                              (ship/has-equipment? equip/is-any-daihatsu? attacking-ship) (* 1.8)
                                              (ship/has-equipment? equip/is-toku-daihatsu? attacking-ship) (* 1.15)
                                              (ship/has-equipment? equip/is-toku-daihatsu-with-11th? attacking-ship) (* 1.8)
                                              (ship/has-equipment? equip/is-daihatsu-with-tank? attacking-ship) (* 1.5)
                                              (<= 2 (ship/count-equipment equip/is-daihatsu-with-tank? attacking-ship)) (* 1.4)
                                              (true) (* (Math/min 1 (installation-bonus-1 attacking-ship)))
                                              (ship/has-equipment? equip/is-landing-tank? attacking-ship) (* 2.4 (installation-bonus-3 attacking-ship))
                                              (<= 2 (ship/count-equipment equip/is-landing-tank? attacking-ship)) (* 1.35)
                                              (ship/has-equipment? equip/is-ap-shell? attacking-ship) (* 1.85)
                                              (ship/has-equipment? equip/is-seaplane-fighter-or-bomber? attacking-ship) (* 1.5)
                                              (ship/has-equipment? equip/is-dive-bomber? attacking-ship) (* 1.5))
    (= :island defending-ship-type) (cond-> 1
                                            (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 2.1)
                                            (= 1 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.4)
                                            (ship/has-equipment? equip/is-mortar-incl-concentrated? attacking-ship) (* 1.2)
                                            (<= 2 (ship/count-equipment equip/is-mortar-incl-concentrated? attacking-ship)) (* 1.4)
                                            (ship/has-equipment? equip/is-type-4-rocket? attacking-ship) (* 1.3)
                                            (<= 2 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (* 1.65)
                                            (ship/has-equipment? equip/is-any-daihatsu? attacking-ship) (* 1.8)
                                            (ship/has-equipment? equip/is-toku-daihatsu? attacking-ship) (* 1.15)
                                            (ship/has-equipment? equip/is-toku-daihatsu-with-11th? attacking-ship) (* 1.8)
                                            (ship/has-equipment? equip/is-daihatsu-with-tank? attacking-ship) (* 1.2)
                                            (<= 2 (ship/count-equipment equip/is-daihatsu-with-tank? attacking-ship)) (* 1.4)
                                            (true) (* (Math/min 1 (installation-bonus-1 attacking-ship)))
                                            (ship/has-equipment? equip/is-landing-tank? attacking-ship) (* 2.4 (installation-bonus-3 attacking-ship))
                                            (<= 2 (ship/count-equipment equip/is-landing-tank? attacking-ship)) (* 1.35))
    (= :northernmost defending-ship-type) (cond-> 1
                                                  (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 2.1)
                                                  (= 1 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.4)
                                                  (ship/has-equipment? equip/is-toku-daihatsu? attacking-ship) (* 1.8)
                                                  (ship/has-equipment? equip/is-toku-daihatsu-with-11th? attacking-ship) (* 2.2)
                                                  (ship/has-equipment? equip/is-landing-tank? attacking-ship) (* 1.75))
    (ship/is-installation? defending-ship-type) (cond-> 1
                                                        (ship/has-equipment? equip/is-type-3-shell? attacking-ship) (* 2.5)
                                                        (ship/has-equipment? equip/is-wg? attacking-ship) (* 1.3)
                                                        (<= 2 (ship/count-equipment equip/is-wg? attacking-ship)) (* 1.4)
                                                        (ship/has-equipment? equip/is-mortar-incl-concentrated? attacking-ship) (* 1.2)
                                                        (<= 2 (ship/count-equipment equip/is-mortar-incl-concentrated? attacking-ship)) (* 1.3)
                                                        (ship/has-equipment? equip/is-type-4-rocket? attacking-ship) (* 1.25)
                                                        (<= 2 (ship/count-equipment equip/is-type-4-rocket? attacking-ship)) (* 1.5)
                                                        (ship/has-equipment? equip/is-any-daihatsu? attacking-ship) (* 1.4)
                                                        (ship/has-equipment? equip/is-toku-daihatsu? attacking-ship) (* 1.15)
                                                        (ship/has-equipment? equip/is-toku-daihatsu-with-11th? attacking-ship) (* 1.8)
                                                        (ship/has-equipment? equip/is-daihatsu-with-tank? attacking-ship) (* 1.5)
                                                        (<= 2 (ship/count-equipment equip/is-daihatsu-with-tank? attacking-ship)) (* 1.3)
                                                        (true) (* (Math/min 1 (installation-bonus-1 attacking-ship)))
                                                        (ship/has-equipment? equip/is-landing-tank? attacking-ship) (* 1.5 (installation-bonus-3 attacking-ship))
                                                        (ship/has-equipment? equip/is-seaplane-fighter-or-bomber? attacking-ship) (* 1.2))
    :else 1))

(defn installation-sub-fp-bonus
  [{attacking-ship :attacking-ship
    defending-ship :defending-ship}]
  (if (and (ship/is-sub? attacking-ship) (ship/is-installation? defending-ship))
    30
    0))

(defn ammo-modifier
  [{ammo :kcss/ammo}]
  (if (< ammo 5)
    (* 0.2 ammo)
    1))

(defn special-attack
  [{{special-attacked :special-attacked
     global-id :kcss/global-id} :attacking-ship
    attacking-ship-idx :attacking-ship-idx
    {attacking-ships :kcss/ships
     attacking-formation :kcss/formation} :attacking-fleet
    }]
  (cond
    special-attacked nil
    (and (contains? #{371 376} global-id) (= 0 attacking-ship-idx) (>= (count attacking-ships) 5) (or (= :double-line attacking-formation) (= :cruising-formation-2 attacking-formation))
         (ship/health-percentage-greater-than? attacking-ships :chuuha) (empty? (filter ship/is-sub? attacking-ships)) (not (or (ship/is-cv? (nth attacking-ships 2)) (ship/is-cv? (nth attacking-ships 4))))
         (not (or (get (nth attacking-ships 2) :retreated false) (get (nth attacking-ships 4) :retreated false)))) (let [roll (rand)]
                                                                                                                         (if (> 0.6 roll)
                                                                                                                           {:attack :nelson-touch
                                                                                                                            :attacking-ship-indexes [0 2 4]
                                                                                                                            :special-attack-roll roll}
                                                                                                                           {:attack nil
                                                                                                                            :special-attack-roll roll}))
    (and (contains? #{341 373} global-id) (= 0 attacking-ship-idx) (>= (count attacking-ships) 2) (or (= :echelon attacking-formation) (= :cruising-formation-2 attacking-formation))
         (ship/health-percentage-greater-than? attacking-ships :chuuha) (ship/is-bb? (nth attacking-ships 1))
         (ship/health-percentage-greater-than? (nth attacking-ships 1) :taiha)) (let [roll (rand)]
                                                                                      (if (> 0.6 roll)
                                                                                        {:attack :naga-mutsu-touch
                                                                                         :attacking-ship-indexes [0 1]
                                                                                         :special-attack-roll roll}
                                                                                        {:attack nil
                                                                                         :special-attack-roll roll}))
    (and (contains? #{401 406} global-id) (= 0 attacking-ship-idx) (>= (count attacking-ships) 3) (or (= :echelon attacking-formation) (= :cruising-formation-2 attacking-formation))
         (every? #(ship/health-percentage-greater-than? (nth attacking-ships %) :chuuha) (range 3)) (every? #(ship/is-bb? (nth attacking-ships %)) (range 1 2))) (let [roll (rand)]
                                                                                                                                                                       (if (> 0.6 roll)
                                                                                                                                                                         {:attack :colorado-touch
                                                                                                                                                                          :attacking-ship-indexes [0 1 2]
                                                                                                                                                                          :special-attack-roll roll}
                                                                                                                                                                         {:attack nil
                                                                                                                                                                          :special-attack-roll roll}))
    :else nil))

(defn get-attack
  [{air-state :kcss/air-state
    {{luck :kcss/luck} :kcss/ship-stats
     :as attacking-ship} :attacking-ship
    attacking-fleet :attacking-fleet
    defending-ship :defending-ship}]
  (if (contains? #{:as :as+} air-state)
    (if-let [attack-types (ship/attack-types attacking-ship)]
      (let [attack-types (if (ship/is-installation? defending-ship) (filter #(not (contains? #{:cvci-fba :cvci-bba :cvci-ba} %)) attack-types) attack-types)
            los (fleet/los attacking-fleet)
            los-modifier (Math/floor (+ (Math/sqrt los) (* los 0.1)))
            luck-modifier (Math/floor (+ (Math/sqrt luck) 10))
            cut-in-chance (as-double-percentile (if (= :as+ air-state)
                                                  (Math/floor (+ luck-modifier 10 (* 0.7 (+ (* (ship/stat attacking-ship :kcss/los) 1.6) los-modifier))))
                                                  (Math/floor (+ luck-modifier (* 0.6 (+ (* (ship/stat attacking-ship :kcss/los) 1.2) los-modifier))))))
            lazy-rolls (map rand (range (count attack-types)))]
        (if-let [cut-in-index (first (keep-indexed #(when (< %2 cut-in-chance) %1) lazy-rolls))]
          (let [cut-in (nth attack-types cut-in-index)
                rolls (reverse (take (inc cut-in-index) lazy-rolls))
                cut-in-roll (first rolls)
                failed-rolls (reverse (rest rolls))]
            {:attack cut-in
             :roll cut-in-roll
             :attack-rolls (merge (map-indexed #({:roll %2
                                                  :attack (nth attack-types %1)}) failed-rolls))})
          {:attack :single-attack
           :attack-rolls (merge (map-indexed #({:roll %2
                                                :attack (nth attack-types %1)}) lazy-rolls))})))
    {:attack :single-attack}))

(defn evasion
  [{{{luck :kcss/luck} :kcss/ship-stats
     {fuel :kcss/fuel} :kcss/resources
     :as defending-ship} :defending-ship
    formation :defending-formation
    :as attack-definition}]
  (let [evasion (ship/stat defending-ship :kcss/evasion)
        base-evasion (* (+ evasion (Math/sqrt (* luck 2))) (get shelling-evasion-modifier formation))
        dodge (cond
                (> base-evasion 65) (+ 55 (* 2 (Math/sqrt (- base-evasion 65))))
                (> base-evasion 40) (+ 40 (* 3 (Math/sqrt (- base-evasion 40))))
                :else base-evasion)
        fuel-penalty (if (< fuel 7.5) (/ (- 7.5 fuel) 10) 0)]
    (+ (as-double-percentile (- dodge fuel-penalty)) (as-double-percentile (vanguard-evasion-bonus attack-definition)))))

(defn accuracy
  [{{morale :kcss/morale
     {luck :kcss/luck} :kcss/ship-stats
     level :kcss/level
     :as attacking-ship} :attacking-ship
    attack :attack
    :as attack-definition}]
  (let [base-accuracy (base-accuracy attack-definition)
        accuracy (ship/stat attacking-ship :accuracy)
        pt-imp-mod (pt-imp-accuracy-modifier attack-definition)
        ap (ap-accuracy attack-definition)
        attack-accuracy (attack-accuracy attack)
        morale-accuracy (morale-shelling-accuracy morale)
        formation-accuracy (shelling-formation-accuracy attack-definition)
        fit-accuracy (ship/fit-accuracy attacking-ship)]
    (* (+ (as-double-percentile (* (+ base-accuracy (* 2 (Math/sqrt level)) (Math/sqrt (* luck 1.5)) accuracy)
                                   (* morale-accuracy formation-accuracy pt-imp-mod)))
          (as-double-percentile fit-accuracy))
       ap
       attack-accuracy)))

(defn hit-type
  [{{morale :kcss/morale} :defending-ship
    attacking-ship :attacking-ship
    crit-modifier :crit-modifier
    crit-bonus :crit-bonus
    :as attack-definition}]
  (let [evasion (evasion attack-definition)
        accuracy (accuracy attack-definition)
        morale-evasion (morale-evasion-modifier morale)
        plane-accuracy (ship/plane-accuracy attacking-ship)
        plane-crit (ship/plane-crit-chance attacking-ship)
        hit-chance (+ (Math/min (* (Math/max (- accuracy evasion) 0.1) morale-evasion) 0.96) (as-double-percentile plane-accuracy))
        crit-chance (+ (as-double-percentile (* (Math/sqrt (* 100 hit-chance)) crit-modifier)) (as-double-percentile plane-crit) crit-bonus)
        roll (/ (Math/floor (* (rand) 100)) 100)]
    {:hit-type (cond
                 (<= roll crit-chance) :crit
                 (<= roll hit-chance) :hit
                 :else :miss)
     :hit-roll roll}))

(defn cvci-hit-bonus
  [attacking-ship]
  (let [base (+ 1 (/ (* 0.1 (ship/plane-accuracy attacking-ship)) 12.46))]
    (cond
      (= (ship/plane-rank attacking-ship 0) 7) (+ base 0.15)
      (= (ship/plane-rank attacking-ship 0) 6) (+ base 0.1)
      :else base)))

(defn hit-bonus
  [{attack :attack
    attacking-ship :attacking-ship
    hit-type :hit-type}]
  (if (= :crit hit-type)
    (let [bonus (cond
                  (contains? #{:cvci-fba :cvci-bba :cvci-ba} attack) (cvci-hit-bonus attacking-ship)
                  (= attack :double-attack) 1
                  :else (ship/plane-crit-damage attacking-ship))]
      (* 1.5 bonus))
    1))

(defn base-damage
  [{formation :attacking-formation
    engagement :engagement
    attacking-ship :attacking-ship
    attack-type :attack-type
    attacking-ship-bonus :attacking-ship-bonus
    hit-bonus :hit-bonus
    :as damage-definition}]
  (let [formation-damage (get shelling-formation-damage-modifier formation)
        engagement-damage (get engagement-damage-modifier engagement)
        ship-damage (ship/stat attacking-ship :kcss/damage)
        ap (ap-damage damage-definition)
        pt-imp (pt-imp-damage-modifier damage-definition)
        attack-damage (attack-damage-modifier damage-definition)
        fit-damage (ship/fit-damage attacking-ship)
        anchorage-damage (anchorage-damage-modifier damage-definition)
        base-cap (attack-base-damage-cap attack-type)
        shell-bonus (shell-bonus damage-definition)
        shelling-power (ship/stat attacking-ship :kcss/shelling-power)
        firepower (ship/stat attacking-ship :kcss/firepower)
        installation-flat-bonus (installation-flat-bonus damage-definition)
        supply-depot-damage (supply-depot-damage-modifier damage-definition)
        installation-type-bonus (installation-type-bonus damage-definition)
        sub-installation-fp-bonus (installation-sub-fp-bonus damage-definition)
        pre (* formation-damage engagement-damage ship-damage)
        post (* ap pt-imp attack-damage anchorage-damage supply-depot-damage)
        shell-power (+ (* (+ firepower sub-installation-fp-bonus) installation-type-bonus) installation-flat-bonus shell-bonus shelling-power fit-damage)
        pre-cap-damage (* shell-power pre)
        post-cap-damage (if (> pre-cap-damage base-cap)
                          (+ base-cap (Math/sqrt (- pre-cap-damage base-cap)))
                          pre-cap-damage)]
    (* post-cap-damage post attacking-ship-bonus hit-bonus)))

(defn scratch-damage
  [{{health :kcss/current-health} :defending-ship}]
  (let [roll (rand)]
    {:damage (Math/floor (* health (+ 0.6 (* 0.8 * (Math/floor (* roll health))))))
     :scratch-damage-roll roll}))

(defn damage
  [{attacking-ship :attacking-ship
    defending-ship :defending-ship
    :as damage-definition}]
  (let [base-damage (base-damage damage-definition)
        armor (ship/stat defending-ship :kcss/armor)
        armor-roll (rand)
        total-armor (+ (* 0.7 armor) (* 0.6 (Math/floor (* armor-roll armor))))
        defending-ship-debuff (debuff damage-definition)
        post-armor-damage (- base-damage (- total-armor defending-ship-debuff))
        asw-penetration (ship/asw-penetration attacking-ship)
        ammo-modifier (ammo-modifier attacking-ship)
        final-damage (Math/floor (* (+ post-armor-damage asw-penetration) ammo-modifier))
        armor-info {:armor-roll armor-roll
                    :total-armor total-armor}]
    (merge (if (>= 0 final-damage)
             (scratch-damage damage-definition)
             {:damage final-damage}) armor-info)))
