(ns kc-sortie-simulator.ship
  (:require [clojure.spec.alpha :as s]))

(s/def :kcss/installation? boolean?)
(s/def :kcss/current-health int?)
(s/def :kcss/level int?)
(s/def :kcss/ship-type keyword?)
(s/def :kcss/ship-stats (s/keys :req [:kcss/firepower :kcss/armor :kcss/anti-air :kcss/torpedo :kcss/luck]))
(s/def :kcss/equipment-stats (s/keys :req []))
(s/def :kcss/block-red-t boolean?)
(s/def :kcss/equipment (s/keys :req [:kcss/global-id :kcss/type :kcss/secondary-type :kcss/equipment-stats :kcss/block-red-t]))
(s/def :kcss/equipments (s/coll-of :kcss/equipment))
(s/def :kcss/ship (s/keys :req [:kcss/global-id :kcss/ship-type :kcss/current-health :kcss/ship-stats :kcss/equipments :kcss/level] :opt [:kcss/installation?]))

(def health-type-upper-bound
  {:chouha 0.75
   :chuuha 0.5
   :taiha 0.25})

(defn stat
  [ship stat-type]
  (reduce + (conj (map #(get-in % [:kcss/equipment-stats stat-type] 0) (get ship :kcss/equipments))
                  (get-in ship [:kcss/ship-stats stat-type]))))

(defn fit-accuracy
  [ship]
  0)

(defn fit-damage
  [ship]
  0)

(defn plane-accuracy
  [ship]
  0)

(defn plane-crit-chance
  [ship]
  0)

(defn plane-crit-damage
  [ship]
  1)

(defn plane-rank
  [ship idx]
  0)

(defn range
  [ship]
  :short)

(defn is-sub?
  [{ship-type :kcss/ship-type}]
  (contains? #{:ssv :ss} ship-type))

(defn is-cv?
  [{ship-type :kcss/ship-type}]
  (contains? #{:cv :cvl :cvb} ship-type))

(defn is-alive?
  [{current-health :kcss/current-health}]
  (< 0 current-health))

(defn weak-to-ap?
  [{ship-type :kcss/ship-type}]
  (contains? #{:ca :cav :bb :fbb :bbv :cv :cvb} ship-type))

(defn can-attack?
  [ship range]
  (and (is-alive? ship) (not (get ship :attacked false)) (= range (range ship))))

(def can-asw?
  (constantly false))

(defn has-equipment?
  [pred ship]
  (some pred (:kcss/equipments ship)))

(defn count-equipment
  [pred ship]
  (count (filter pred (:kcss/equipments ship))))

(def has-equipment-type?
  (constantly false))

(def is-installation?
  (constantly false))

(defn is-bb?
  [{ship-type :kcss/ship-type}]
  (contains? #{:bbv :bb :fbb} ship-type))

(defn asw-penetration
  [ship]
  (let [de-bonus (if (= (get ship :kcss/ship-type) :de) 1 0)]
    (reduce + (map
                #(+ (Math/max 0 (Math/sqrt (- (get-in %1 [:kcss/equipment-stats :kcss/asw]) 2))) de-bonus)
                (filter #(= :depth-charge-2 (get % :kcss/secondary-type)) (get ship :kcss/equipments))))))

(defn health-percentage-greater-than?
  [{{health :kcss/health} :kcss/ship-stats
    current-health :kcss/current-health} health-type]
  (let [health-upper-bound (get health-type-upper-bound health-type)]
    (<= health-upper-bound (/ current-health health))))

(defn attack-types
  [ship]
  nil)

(defn ap-type
  [ship]
  (cond
    (every? #(has-equipment-type? ship %) [:ap-shell :main-gun :radar :secondary-gun]) :all-types
    (every? #(has-equipment-type? ship %) [:ap-shell :main-gun :secondary-gun]) :secondary
    (every? #(has-equipment-type? ship %) [:ap-shell :main-gun :radar]) :radar
    (every? #(has-equipment-type? ship %) [:ap-shell :main-gun]) :basic
    :else nil))
