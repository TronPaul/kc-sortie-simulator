(ns kc-sortie-simulator.util
  (:require [clojure.tools.logging :as log]))

(defn flatten-map
  "Transform a nested map into a seq of [keyseq leaf-val] pairs"
  [m]
  (when m
    ((fn flatten-helper [keyseq m]
       (when m
         (cond
           (and (map? m) (not-empty m)) (mapcat (fn [[k v]] (flatten-helper (conj keyseq k) v)) m)
           (and (coll? m) (not-empty m)) (apply concat (map-indexed (fn [k v] (flatten-helper (conj keyseq k) v)) m))
           :else [[keyseq m]])))
     [] m)))

(defn assocv-in
  "Associates a value in a nested associative structure, where ks is a
  sequence of keys and v is the new value and returns a new nested structure.
  If any levels do not exist, hash-maps will be created."
  {:added "1.0"
   :static true}
  [m [k & ks] v]
  (if (and (= k 0) (nil? m))
    (let [mv (vec m)]
      (if ks
        (conj mv (assocv-in (get mv k) ks v))
        (conj mv v)))
    (if ks
      (assoc m k (assocv-in (get m k) ks v))
      (assoc m k v))))

(defn unflatten
  "Transform a seq of [keyseq leaf-val] pairs into a nested map.
   If one keyseq is a prefix of another, you're on your own."
  [s]
  (reduce (fn [m [ks v]] (if (seq ks) (assocv-in m ks v) v)) {} s))

(defmacro cond->->
  "Takes an expression and a set of test/form pairs. Threads expr (via ->)
  through each test and form for which the corresponding test
  expression is true. Note that, unlike cond branching, cond->-> threading does
  not short circuit after the first true test expression."
  {:added "1.5"}
  [expr & clauses]
  (assert (even? (count clauses)))
  (let [g (gensym)
        steps (map (fn [[test step]] `(if (-> ~g ~test) (-> ~g ~step) ~g))
                   (partition 2 clauses))]
    `(let [~g ~expr
           ~@(interleave (repeat g) (butlast steps))]
       ~(if (empty? steps)
          g
          (last steps)))))
