(defproject kc-sortie-simulator "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.logging "0.5.0"]]
  :main ^:skip-aot kc-sortie-simulator.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
