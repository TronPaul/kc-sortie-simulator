# Motivation

I started playing KC in September 2017 finally succumbing to the cajoling of a friend. Ever since I've
been learning how the game works and still feel like I'm an idiot about the game a lot of the time. There
are some resources out there for how the game works but information is scattered and difficult to parse.
The Fall 2019 event finally annoyed me enough to want to understand the game better and build something
to help me plan out my events. The biggest frustration is not fragging the boss.

There exists 2 simulators that I know of [kancolle-replay](https://github.com/KC3Kai/kancolle-replay)
and [KanColleSimulator](https://github.com/YSRKEN/KanColleSimulator). KanColleSimulator hasn't been 
updated in 4 years. Neither are written in languages I enjoy to use and neither are what I'm looking
for. For that reason, I've ended up falling for the standard software developer trap: Not Built Here.

I make no guarantees that I'll ever get close to finishing, but it should be an interesting ride. If I
get something wrong feel free to let me know. If you have a source for how that portion of the game works
please link it! I can't read, let alone speak Japanese so everything I'll be looking at will be from
English sources.
